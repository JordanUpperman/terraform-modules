module "lambda" {
  source = "../lambda"
  function_name = var.function_name
  filename = var.filename
  extension = var.extension
  path = var.path
  runtime = var.runtime
  environment_variables = var.environment_variables
}

resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda.lambda_arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${var.parent_api_execution_arn}/*/*"
  depends_on = [
    module.lambda
  ]
}

resource "aws_apigatewayv2_integration" "lambda" {
  api_id           = var.parent_api_id
  integration_type = "AWS_PROXY"
  integration_uri      = module.lambda.invoke_arn
  payload_format_version = "2.0"
}

resource "aws_apigatewayv2_route" "main" {
  api_id        = var.parent_api_id
  route_key = "${var.http_method} ${var.api_endpoint}"
  target        = "integrations/${aws_apigatewayv2_integration.lambda.id}"
  authorization_type = "NONE"
  depends_on = [
    aws_apigatewayv2_integration.lambda
  ]
}
