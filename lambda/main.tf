locals {
  discard_folder="${path.module}/../temp/${var.function_name}-"
}

# data "archive_file" "node_modules_zip" {
#   type        = "zip"
#   excludes = [
#     "index.js"
#   ]
#   source_dir  = "${var.path}"
#   output_path = "${local.discard_folder}lambda-layer.zip"
# }

# resource "aws_lambda_layer_version" "node_modules" {
#   filename   = "${data.archive_file.node_modules_zip.output_path}"
#   layer_name = "${var.function_name}-node_modules"

#   compatible_runtimes = ["${var.runtime}"]
# }

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "${var.path}/${var.filename}.${var.extension}"
  output_path = "${local.discard_folder}lambda.zip"
}

resource "aws_lambda_function" "main" {
  filename         = data.archive_file.lambda_zip.output_path
  function_name    = var.function_name
  role             = aws_iam_role.lambda.arn
  handler          = "${var.function_name}/${var.filename}.handler"
  source_code_hash = filebase64sha256(data.archive_file.lambda_zip.source_file)
  runtime          = var.runtime
  timeout          = 30
  memory_size      = 128
  publish          = true
  # layers =[
  #   aws_lambda_layer_version.node_modules.arn
  # ]
  environment {
    variables = var.environment_variables
  }
  depends_on = [ 
    data.archive_file.lambda_zip
   ]
}

resource "aws_iam_role" "lambda" {
  name = "${var.filename}_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy" "lambda_logging" {
  name   = "${var.filename}_logging_policy"
  role   = aws_iam_role.lambda.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        Effect   = "Allow",
        Resource = "arn:aws:logs:*:*:*"
      },
    ]
  })
}