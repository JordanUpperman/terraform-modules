variable "function_name" {
  type = string
  description = "The name of the lambda"
}
variable "filename" {
  type        = string
  description = "The name of the file in the dist.  Do not include path."
}
variable "extension" {
  type        = string
  default = "js"
  description = "The name of the file in the dist.  Do not include path."
}

variable "path" {
  type = string
  description = "The directory of the file for the lambda"
}
variable "runtime" {
  type        = string
  default     = "nodejs20.x"
  description = "The runtime for the Lambda function (e.g., python3.8, nodejs14.x)."
}

variable "environment_variables" {
  type        = map(string)
  description = "Environment variables for the Lambda function."
  default     = {}
}
