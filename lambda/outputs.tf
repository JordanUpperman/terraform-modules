output "lambda_arn" {
  value = aws_lambda_function.main.arn
}

output "invoke_arn" {
  value = aws_lambda_function.main.invoke_arn
}

output "role_id" {
  value = aws_iam_role.lambda.id
}